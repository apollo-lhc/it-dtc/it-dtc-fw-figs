#!/bin/bash

drawio -x -o ./output/Overview.png             --crop -p  0 IT-DTC.drawio
drawio -x -o ./output/Trigger_Distribution.png --crop -p  1 IT-DTC.drawio
drawio -x -o ./output/LPGBT_Grouping.png       --crop -p  2 IT-DTC.drawio
drawio -x -o ./output/CMD_link_mapper.png      --crop -p  3 IT-DTC.drawio
drawio -x -o ./output/Chip_mapper.png          --crop -p  4 IT-DTC.drawio
drawio -x -o ./output/Tag_Manager.png          --crop -p  5 IT-DTC.drawio
drawio -x -o ./output/Downling_builder.png     --crop -p  6 IT-DTC.drawio
drawio -x -o ./output/Stream_Decoder.png       --crop -p  7 IT-DTC.drawio
drawio -x -o ./output/Fragment_Selector.png    --crop -p  8 IT-DTC.drawio
drawio -x -o ./output/Fragment_Streamer.png    --crop -p  9 IT-DTC.drawio
drawio -x -o ./output/Event_Fragment_FIFO.png  --crop -p 10 IT-DTC.drawio
drawio -x -o ./output/Event_Builder.png        --crop -p 11 IT-DTC.drawio
drawio -x -o ./output/Data_format_simple.png   --crop -p 12 IT-DTC.drawio
drawio -x -o ./output/Readout_Chain_TFPX.png   --crop -p 15 IT-DTC.drawio
